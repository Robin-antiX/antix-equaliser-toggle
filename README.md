# antiX equaliser toggle

antiX Equaliser Toggle is a GUI helper script, a tool for activating or deactivating pipewire-equaliser »Easyeffects« in antiX control centre and managing it's startup entry for bringing up the equaliser in deamonised service mode at antiX startup.

## Installation

A.) Apt package installation (This is the preferred way of installation.)
- [ ] Download deb-installer-package and checksum file from respective folder. It was tested on antiX 23, 32bit and 64bit both.
- [ ] Check shasum file using the command:
```
shasum -c './antix-equaliser-toggle.sha512.sum'
```
It must show an _OK_.
- [ ] If check was OK, install the package by the commands 
```
sudo apt-get update && sudo apt-get install './antix-equaliser-toggle.deb'
```
Check console output for error messages during installation.

B.) Manual installation:
- [ ] Put the script file _antix-equaliser-toggle_ from _bin_ directory into the antiX system folder _/usr/local/bin_ and make it executable (_sudo chmod 755 /usr/local/bin/antix-equaliser-toggle_). Hint: script won't work properly if not living in a folder included into PATH environment variable for executables.
- [ ] Put the localisation of its user interface for your language (_.mo_ type file) into the respective locale subflolder of your antiX system. ( _/usr/share/locale/‹your_language_ID›/LC_MESSAGES_ ).
- put the easyeffects-toggle.png icon file into the /usr/share/icons/antix-papirus directory of your system.
- [ ] Make sure you have _easyeffects_, _wireplumber_, _xdotool_, _wmctrl_, _yad_, _grep_ and _gettext_ installed.

## Usage
Enter _antiX-equaliser-toggle into a command line window, e.g. RoxTerm.
In the GUI coming up click read the tooltips and flip switches as needed. Changes apply immediately. Click leave button when done.

## Known Issues
- When running on other system font sizes than the ones originally used while creating the GUI or it's translations the symbol icons won't line up properly in GUI.
If somebody knows of a method to align the icons left instead of right within yad field buttons to avoid the dirty workaround appied, filling up the line ends with blanks, please post in www.antixforum.com
- Works for pipewire equaliser Easyeffects merely by now. Future versions to come with plain alsa equaliser support to match antiX's freedom of choice.

## Support
For help, questions, suggestions and bug reporting please write to [antiXlinux forums](https://www.antixforum.com).

## Contributing
- If you find a translation of user interface or user manual being inadequate or misleading wrong, please edit it simply on [antix-development at transifex](https://www.transifex.com/anticapitalista/antix-development/antix-equaliser-toggle/). On transifex you may edit the string translations used in user interface directly.

## Authors and acknowledgment
This is an antiX community project.

## License
GPL Version 3 (GPLv3)

-----------
Written 2024 by Robin.antiX for antiX community.


