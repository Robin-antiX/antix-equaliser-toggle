��          �            h     i  %   n  �   �  �     �   �  �   S     �  �   �     �     �     �  ?   �       '     ;  ?  	   {  $   �  �   �  �   \  �     �   �  *   l	    �	     �
     �
     �
  >   �
       .                                  	                                    
              Done Easyeffects antiX startup config      Easyeffects equaliser is disabled or not present in antiX startup configuration file. Flip switch to add or change config entry. Easyeffects equaliser is not running in service daemon mode. Will be started when switch is flipped. Any GUI instance of Easyeffects will be reinitialised. Easyeffects equaliser is running in service daemon mode. Will be stopped when switch is flipped. Any GUI instance of easyeffects will be shut down the same time. Easyeffects equaliser is set to start it’s daemonised service mode on antiX startup. Flip switch to disable startup config entry. Easyeffects service daemon mode Leaves this window. On leaving (even when pressing ESC button), the changes made will be kept last state, and have been applied the moment you’ve flipped one of the switches. No Pipewire. OK Pipewire not running. Please go to antiX control centre\n\tand start Pipewire before. Warning! antiX easyeffects service daemon toggle Project-Id-Version: antiX-equaliser-toggle
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-19 22:38+0100
Last-Translator: 
Language-Team: antiX de translation team (www.antixforum.com)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.2.2
 Verlassen Easyeffects antiX Startkonfiguration Der Easyeffects Equaliser Hintergrunddienst wird derzeit nicht automatisch beim Systemstart geladen. Umlegen des Schalters trägt den Dienst in die antiX Startkonfiguration ein. Der Easyeffects Equaliser läuft nicht im Hintergrundmodus. Umlegen des Schalters startet den Hintergrunddienst. Vorhandene Easyeffekts-Fenster werden reinitialisiert. Der Easyeffects Equaliser läuft im Hintergrundmodus. Umlegen des Schalters beendet den Hintergrunddienst. Alle Easyeffects-Fenster werden zugleich geschlossen. Der Easyeffects Equaliser Hintergrunddienst wird derzeit automatisch beim Systemstart geladen. Umlegen des Schalters deaktiviert den Eintrag für diesen Dienst in der antiX Startkonfigurationsdatei. Easyeffects Hintergrunddienst              Schließt dieses Fenster.  Beim Verlassen werden alle vorgenommenen Einstellungen so beibehalten, wie sie zu sehen sind, auch wenn Sie die ESC-Taste verwenden. Alle Änderungen wurden bereits in dem Augenblick ausgeführt, in dem Sie die Schalter umgelegt haben. Pipewire nicht gefunden. OK Piwewire ist nicht gestartet. Bitte starten Sie zuerst Pipewire\n\tim antiX Kontrollzentrum. Achtung! antiX easyeffects Hintergrunddienst-Verwaltung 